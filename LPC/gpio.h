#ifndef _GPIO_H_
#define _GPIO_H_

//LEDs da LPC Xpresso Base board
#define LED_RED_PORT    1                 //GPIO_28  (PIO1_9)
#define LED_RED_MASK    (1 << 9)          //GPIO_28  (PIO1_9)

#define LED_BLUE_PORT    1                 //GPIO_14  (PIO1_2)
#define LED_BLUE_MASK    (1 << 2)          //GPIO_14  (PIO1_2)

#define LED_GREEN_PORT    1                 //GPIO_29  (PIO1_10)
#define LED_GREEN_MASK    (1 << 10)          //GPIO_29  (PIO1_10)

#define SW3_PORT          2                     //GPIO_37
#define SW3_MASK          (1 << 9)

#define SW4_PORT          1                     //GPIO_16
#define SW4_MASK          (1 << 4)




extern volatile int* IOCON_R_PIO1_2;
extern volatile int* IOCON_PIO1_10;
extern volatile int* IOCON_PIO1_9;
extern volatile int* IOCON_PIO2_9;
extern volatile int* IOCON_PIO0_1 ;
extern volatile int* IOCON_PIO0_2;
extern volatile int* IOCON_PIO0_3;
extern volatile int* IOCON_PIO1_8;
extern volatile int* IOCON_PIO2_5;
extern volatile int* IOCON_PIO2_6;
extern volatile int* IOCON_PIO2_7;
extern volatile int* IOCON_PIO2_8;


void config_saida(int port, int output_mask);


void escrever(int port, int mask, int value);
void escrever2 (int port, int value);

int ler(int port, int mask);
int ler2(int port);


#endif