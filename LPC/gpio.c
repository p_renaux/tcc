#include "system_LPC13xx.h"             //SystemCoreClockUpdate
                    //atualiza vari�vel global SystemCoreClock -> utilizar nos drivers

#include "lpc13xx.h"                    //registradores

#include "core_cm3.h"           //perif�ricos do n�cleo (particularmente NVIC)


#include <stdio.h>                     //printf

#include "gpio.h"

volatile int* GPIODATA_BASE[3] = {(int*)0x50000000, 
                          (int*)0x50010000,
                          (int*)0x50020000 };

int GPIODIR_OFFSET = 0x8000;

volatile int* IOCON_R_PIO1_2 = (int*)0x40044080;
volatile int* IOCON_PIO1_10 = (int*)0x4004406C;
volatile int* IOCON_PIO1_9 = (int*)0x40044038;
volatile int* IOCON_PIO2_9 = (int*)0x40044054;


//inputs and outputs
volatile int* IOCON_PIO0_1 = (int*)0x40044010;
volatile int* IOCON_PIO0_2 = (int*)0x4004401C;
volatile int* IOCON_PIO0_3 = (int*)0x4004402C;
volatile int* IOCON_PIO1_8 = (int*)0x40044014;
volatile int* IOCON_PIO2_5 = (int*)0x40044044;
volatile int* IOCON_PIO2_6 = (int*)0x40044000;
volatile int* IOCON_PIO2_7 = (int*)0x40044020;
volatile int* IOCON_PIO2_8 = (int*)0x40044024;
  


void config_saida(int port, int output_mask)
{
  *((volatile int*)((int)GPIODATA_BASE[port] + GPIODIR_OFFSET)) = output_mask;
  
}


void escrever(int port, int mask, int value)
{
    //m�scara � utilizada para calcular o endere�o de GPIODATAMASK
    //Shift de 2 para esquerda ocorre automaticamente porque o ponteiro � para int (32 bits)
    volatile int* address = GPIODATA_BASE[port] + mask;
      
    *address = value;
  
}

void escrever2 (int port, int value)
{
    volatile int* address = GPIODATA_BASE[port] + (0x3ffc >> 2);
    *address = value;
}

int ler(int port, int mask)
{
    //m�scara � utilizada para calcular o endere�o de GPIODATAMASK
    //Shift de 2 para esquerda ocorre automaticamente porque o ponteiro � para int (32 bits)
    volatile int* address = GPIODATA_BASE[port] + mask;
      
    return *address;
  
}

int ler2 (int port)
{
    volatile int* address = GPIODATA_BASE[port] + (0x3ffc >> 2);
    return *address;
}