#include "system_LPC13xx.h"             //SystemCoreClockUpdate
                    //atualiza vari�vel global SystemCoreClock -> utilizar nos drivers

#include "lpc13xx.h"                    //registradores

#include "core_cm3.h"           //perif�ricos do n�cleo (particularmente NVIC)


#include <stdio.h>                     //printf

#include "gpio.h"

typedef struct 
{
  int port;
  int index;
} pin;

pin inputs[4] = {{0, 1}, {0,2}, {0,3}, {1,8}};
pin outputs[4] = {{2, 5}, {2, 6}, {2,7}, {2,8}};

//Entradas
//PIO0_1 -> desligar jumper GPIO24
//PIO0_2 -> desligar jumper GPIO4 (J25) e jumper 3-4 de J43
//PIO0_3 -> desligar jumper GPIO25 (J12)
//PIO1_8 -> desligar J18 e J37

//Sa�das
//PIO2_5 -> todos desligados por default
//PIO2_6 -> todos desligados por default
//PIO2_7 -> desligar jumper 1-2 de J43
//PIO2_8 -> todos desligados por default


//defini��es dos modos

#define MODE_0          0                  //4 chaves + 4 lampadas
#define MODE_1          1                  //PIO0_1 � chave geral (quando pressionada por aprox. 1 s)
#define MODE_2          2                  //ar condicionado

#define MIN_INPUT        0
#define MAX_INPUT        3

int g_last_value;               //�ltimo valor das entradas (para modo 1)
int g_time_pressed = 0;
int g_turned_off_mode_1 = 0;
int g_output_value_bkp = 0;


int lerEntradas();
void processa(int in_value, int mode);

void main ()
{
  
    int mode = 0;               //default
   
  
    //configura pinos dos LEDs e bot�o para GPIO
    *IOCON_R_PIO1_2 = 0x1;
    *IOCON_PIO1_10 = 0x0;
    *IOCON_PIO1_9 = 0x0;
    *IOCON_PIO2_9 = 0x0;

  //configura entradas e sa�das do controlador
   *IOCON_PIO0_1 = 0;
   *IOCON_PIO0_2 = 0;
   *IOCON_PIO0_3 = 0;
   *IOCON_PIO1_8 = 0;
   *IOCON_PIO2_5 = 0;
   *IOCON_PIO2_6 = 0;
   *IOCON_PIO2_7 = 0;
   *IOCON_PIO2_8 = 0;

    
  
    //configura LEDs como sa�das
    config_saida(1, LED_RED_MASK | LED_BLUE_MASK | LED_GREEN_MASK);
    
   //configurar sa�das
    config_saida(2, (1 << 5) + (1 <<6) + (1<<7) + (1<<8));
    
     //desliga todas as l�mpadas independentemente dos interruptores
            escrever2(2, 0);
    
    
//    //acender e apagar LED vermelho
//    escrever(LED_RED_PORT, LED_RED_MASK, LED_RED_MASK);
//    escrever(LED_RED_PORT, LED_RED_MASK, 0);
//    
//    //acender e apagar LED azul
//    escrever(LED_BLUE_PORT, LED_BLUE_MASK, LED_BLUE_MASK);
//    escrever(LED_BLUE_PORT, LED_BLUE_MASK, 0);
//    
//    //acender e apagar LED verde
//    escrever(LED_GREEN_PORT, LED_GREEN_MASK, LED_GREEN_MASK);
//    escrever(LED_GREEN_PORT, LED_GREEN_MASK, 0);
    
    int sw_ok = 1;
    //testar SW3
    while (1)
    {
      
        //c�digo "is alive" e mudan�a de modo
      
        if(ler(SW3_PORT, SW3_MASK) != 0 && sw_ok == 0)
        {
            sw_ok = 1;
        }
      
       if(ler(SW3_PORT, SW3_MASK) == 0 && sw_ok == 1)
       {
         
          mode = (mode + 1) % 3;
          sw_ok = 0;
         
          //apagar todos os LEDs
          escrever(LED_RED_PORT, LED_RED_MASK, 0);
          escrever(LED_BLUE_PORT, LED_BLUE_MASK, 0);
          escrever(LED_GREEN_PORT, LED_GREEN_MASK, 0);       
          
          switch (mode)            
          {
          case 0:
              escrever(LED_RED_PORT, LED_RED_MASK, LED_RED_MASK);
              break;
          case 1:
              escrever(LED_BLUE_PORT, LED_BLUE_MASK, LED_BLUE_MASK);
              break;
          case 2:
              escrever(LED_GREEN_PORT, LED_GREEN_MASK, LED_GREEN_MASK);
              break;
              
              
          }
            //printf("\nSW3 pressionada");
           //acender e apagar LED vermelho
//            escrever(LED_RED_PORT, LED_RED_MASK, LED_RED_MASK);
//          for (int i = 0; i < 5000000; i++);
//          escrever(LED_RED_PORT, LED_RED_MASK, 0);
     
          //acender e apagar LED azul
//          escrever(LED_BLUE_PORT, LED_BLUE_MASK, LED_BLUE_MASK);
//          for (int i = 0; i < 5000000; i++);
//          escrever(LED_BLUE_PORT, LED_BLUE_MASK, 0);
//    
//          //acender e apagar LED verde
//          escrever(LED_GREEN_PORT, LED_GREEN_MASK, LED_GREEN_MASK);
//          for (int i = 0; i < 5000000; i++);
//          escrever(LED_GREEN_PORT, LED_GREEN_MASK, 0);
                    
       }
       
       //testa entradas
       int value = lerEntradas();
       
       processa(value, mode);
       
      
    }
    
}

int lerEntradas()
{
    int res = 0;
    for (int i = MIN_INPUT ; i <= MAX_INPUT; i++)
    {
      //res = res << 1;
      res = res + ((ler(inputs[i].port, (1 << inputs[i].index)) ? 1: 0) << i);
        
    }
    return res;
}

void processa(int in_value, int mode)
{
    int cur_output_value = 0;
    switch (mode)
    {
    case 0:
      cur_output_value = ((ler2(2) >> 5) & 0xF);
     
      for (int i = MIN_INPUT; i <= MAX_INPUT; i++)
      {
        //se mudou de 0 para 1
        if ((in_value & (1 << i)) != 0 && ((g_last_value & (1 << i))==0))
        {
          
          // printf("\nread input = %x", in_value);
          // printf("\nCur output value: %x", cur_output_value);
          //xor do valor atual da sa�da com 1 -> inverte a sa�da          
          cur_output_value = cur_output_value ^ (1 << i);
          
          //transp�e as entradas para as sa�das (PIN2_5 a PIN2_8)          
          escrever2(2, (cur_output_value << 5));
          //printf("\nNew output value: %x", cur_output_value);
        }
        
      }
      break;
    case 1:
      //chave geral em PIO0_1 (ou chave mais � esquerda)
      //enquanto estiver pressionada, aumenta o contador
      if ((in_value & (0x1 << MIN_INPUT)) != 0)
      {
        
          if ((g_last_value & (0x1 << MIN_INPUT)) != 0)
          {
            //continua pressionando, incrementa o contador de pressionamento
            g_time_pressed++;
          }
          else
          {
            //acabou de pressionar, modo de chave geral ligada
            if (g_turned_off_mode_1 == 1)
            {
              
                 //transp�e as entradas para as sa�das (PIN2_5 a PIN2_8)          
              escrever2(2, (g_output_value_bkp << 5));
              g_last_value = in_value;
                
            }
              g_turned_off_mode_1 = 0;
          }
          
          //se ficou pressionando tempo suficiente
          if (g_time_pressed == 600000)
          {              
            
               g_output_value_bkp = ((ler2(2) >> 5) & 0xF);
              //desliga todas as l�mpadas independentemente dos interruptores
              escrever2(2, 0);
              g_turned_off_mode_1 = 1;              
             
            
          }
      }
      else 
      {
          g_time_pressed = 0;
      }
      if (g_turned_off_mode_1 == 0)
      {
        int cur_output_value = ((ler2(2) >> 5) & 0xF);
        for (int i = MIN_INPUT; i <= MAX_INPUT; i++)
        {
          //se mudou de 0 para 1
         if ((in_value & (1 << i)) != 0 && ((g_last_value & (1 << i))==0))
          {
            //xor do valor atual da sa�da com 1 -> inverte a sa�da          
            cur_output_value = cur_output_value ^ (1 << i);
            
            //transp�e as entradas para as sa�das (PIN2_5 a PIN2_8)          
            escrever2(2, (cur_output_value << 5));
          }
          
        }
      }  
      break;
    case 2: //n�o implementado
      break;
      
    }
    g_last_value = in_value;
  
}
