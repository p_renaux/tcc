﻿using System;
using System.Collections.Generic;
// using System.Linq;
using System.Text;
// using System.Threading.Tasks;
// DllImport
using System.Runtime.InteropServices;

/* 
 * Recommended Reading:
 *
 * calling dll function     https://msdn.microsoft.com/en-us/library/be80xase.aspx
 *      marshaling data     https://msdn.microsoft.com/en-us/library/fzhhdwae.aspx
 *      unmanaged code      DllImport is used to operate on binary files older than .NET 2.0
 *                          http://www.developer.com/net/cplus/article.php/2197621/Managed-Unmanaged-Native-What-Kind-of-Code-Is-This.htm
 * passing structures       https://msdn.microsoft.com/en-us/library/awbckfbz.aspx
 * imported function returns structure - http://stackoverflow.com/questions/6295171/how-can-i-import-a-dll-function-which-returns-this-structure
 * EXCEPTION:
 *      PInvokeStackImbalance occurred - Additional information: A call to PInvoke function 'ConsoleApplication1!IHIL.HidFunctions::HID_GetOutputReportSize' has unbalanced the stack. This is likely because the managed PInvoke signature does not match the unmanaged target signature. Check that the calling convention and parameters of the PInvoke signature match the target unmanaged signature.
 *      http://stackoverflow.com/questions/2390407/pinvokestackimbalance-c-sharp-call-to-unmanaged-c-function/2738125#comment2825285_2738125
 *      -> use signature [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)] 
 * 
 * Project Properties to be changed:
 * 
 * Build: allow unsafe
 * BadFormatException: verify Build > Platform Target (x86)
 * 
 * 
 */
namespace IHIL
{
    class HidFunctions
    {
        // private static UInt32 DISPOSABLE_ADDRESS = 395123123;
        private static int BOOL_TRUE = 1;

        /// <summary>
        /// Encapsulation internal function of same name to directly return desired String.
        /// </summary>
        /// /// <param name="sz">exact size of desired output string, values equal or lower than zero are substituted to 32.</param>
        public static String GetName(int num, int sz)
        {
            String buf = "";
            sz = sz <= 0 ? 32 : sz;
            HID_GetName(num, buf, sz);
            return buf;
        }

        /// <summary>
        /// Encapsulation internal function of same name to directly return desired String.
        /// </summary>
        /// <param name="sz">exact size of desired output string, values equal or lower than zero are substituted to 32.</param>
        public static String GetFeature(UInt32 sz)
        {
            String buf = "";
            sz = sz <= 0 ? 32 : sz;
            HID_GetFeature(buf, sz);
            return buf;
        }

        /// <summary>
        /// Encapsulation internal function of same name to directly return desired String.
        /// </summary>
        /// /// <param name="sz">exact size of desired output string, values equal or lower than zero are substituted to 32.</param>
        public static String SetFeature(string feature)
        {
            uint sz = (uint)feature.Length;
            sz = sz <= 0 ? 32 : sz;
            HID_GetFeature(feature, sz);
            return feature;
        }

        #region Import from dll
        // to receive a struct from DLL with sequential layout
        // [StructLayout(LayoutKind.Sequential)]
        // public struct BOOL { }
        // ----------------------------
        // add annotation
        //      [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        // to functions that receive parameters

        [DllImport("HIDClient.dll")]
        public static extern void HID_Init();

        [DllImport("HIDClient.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Test();

        [DllImport("HIDClient.dll")]
        public static extern void HID_UnInit();

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_FindDevices();
        
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetInputReportSize(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetOutputReportSize(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetFeatureReportSize(int num);

        [DllImport("HIDClient.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetSelectedDevice();

        [DllImport("HIDClient.dll")]
        public static extern void HID_Close();

        // BOOL  HID_Read (BYTE *buf, DWORD sz, DWORD *cnt);
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_Read (
            [MarshalAs(UnmanagedType.LPStr)] String buf, 
            [MarshalAs(UnmanagedType.U4)] UInt32 sz, 
            [MarshalAs(UnmanagedType.U4)] UInt32 cnt
            );
        
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int HID_Open(int num);
        public static bool Open (int num) { return HID_Open(num) == BOOL_TRUE; }

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        // public static extern BOOL HID_GetName(int num, ref char buf, int sz);
        private static extern int HID_GetName(int num, [MarshalAs(UnmanagedType.LPStr)] String buf, int sz);
        
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int HID_Write(
            [MarshalAs(UnmanagedType.LPStr)] String  buf,
            [MarshalAs(UnmanagedType.U4)] UInt32 sz,
            [MarshalAs(UnmanagedType.U4)] UInt32 cnt
            );

        //public static String HID_Write (String buf, UInt32 sz, UInt32 cnt)
        // { }

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int HID_GetFeature([MarshalAs(UnmanagedType.LPStr)] String  buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz);

        

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int HID_SetFeature([MarshalAs(UnmanagedType.LPStr)] String  buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz);

        #endregion

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevCount();

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int HID_DevSelected();

        // extern "C" __declspec(dllexport) DWORD HID_DevDetailData_CbSize(int num);
        // extern "C" __declspec(dllexport) CHAR* HID_DevDetailData_DevicePath(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevDetailDataSz(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevInputReportSz(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevOutputReportSz(int num);

        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevFeatureReportSz(int num);
    }
}
