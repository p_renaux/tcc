
#ifndef __HID_H__
#define __HID_H__

extern "C" __declspec(dllexport) void  HID_Init();
extern "C" __declspec(dllexport) int   Test();
extern "C" __declspec(dllexport) void  HID_UnInit();
extern "C" __declspec(dllexport) int   HID_FindDevices();
extern "C" __declspec(dllexport) BOOL  HID_GetName(int num, char *buf, int sz);
extern "C" __declspec(dllexport) int   HID_GetInputReportSize(int num);
extern "C" __declspec(dllexport) int   HID_GetOutputReportSize(int num);
extern "C" __declspec(dllexport) int   HID_GetFeatureReportSize(int num);
extern "C" __declspec(dllexport) BOOL  HID_Open(int num);
extern "C" __declspec(dllexport) int   HID_GetSelectedDevice();
extern "C" __declspec(dllexport) void  HID_Close();
extern "C" __declspec(dllexport) BOOL  HID_Read (BYTE *buf, DWORD sz, DWORD *cnt);
extern "C" __declspec(dllexport) BOOL  HID_Write(BYTE *buf, DWORD sz, DWORD *cnt);
extern "C" __declspec(dllexport) BOOL  HID_GetFeature(BYTE *buf, DWORD sz);
extern "C" __declspec(dllexport) BOOL  HID_SetFeature(BYTE *buf, DWORD sz);


extern "C" __declspec(dllexport) int HID_DevCount();
extern "C" __declspec(dllexport) int HID_DevSelected();
extern "C" __declspec(dllexport) DWORD HID_DevDetailData_CbSize(int num);
extern "C" __declspec(dllexport) CHAR* HID_DevDetailData_DevicePath(int num);
extern "C" __declspec(dllexport) int HID_DevDetailDataSz(int num);
extern "C" __declspec(dllexport) int HID_DevInputReportSz(int num);
extern "C" __declspec(dllexport) int HID_DevOutputReportSz(int num);
extern "C" __declspec(dllexport) int HID_DevFeatureReportSz(int num);
#endif /* __HID_H__ */
