//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by HIDClient.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_HIDCLIENT_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_DEVICE                      1000
#define IDC_INPUT0                      1001
#define IDC_INPUT1                      1002
#define IDC_INPUT2                      1003
#define IDC_INPUT3                      1004
#define IDC_INPUT4                      1005
#define IDC_INPUT5                      1006
#define IDC_INPUT6                      1007
#define IDC_INPUT7                      1008
#define IDC_OUTPUT0                     1009
#define IDC_OUTPUT1                     1010
#define IDC_OUTPUT2                     1011
#define IDC_OUTPUT3                     1012
#define IDC_OUTPUT4                     1013
#define IDC_OUTPUT5                     1014
#define IDC_OUTPUT6                     1015
#define IDC_OUTPUT7                     1016
#define IDC_INPUT                       1017
#define IDC_OUTPUT                      1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
