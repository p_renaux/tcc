﻿using System;
using System.Collections.Generic;
// using System.Linq;
using System.Text;
// using System.Threading.Tasks;
// DllImport
using System.Runtime.InteropServices;

/* 
 * Recommended Reading:
 *
 * calling dll function     https://msdn.microsoft.com/en-us/library/be80xase.aspx
 *      marshaling data     https://msdn.microsoft.com/en-us/library/fzhhdwae.aspx
 *      unmanaged code      DllImport is used to operate on binary files older than .NET 2.0
 *                          http://www.developer.com/net/cplus/article.php/2197621/Managed-Unmanaged-Native-What-Kind-of-Code-Is-This.htm
 * passing structures       https://msdn.microsoft.com/en-us/library/awbckfbz.aspx
 * imported function returns structure - http://stackoverflow.com/questions/6295171/how-can-i-import-a-dll-function-which-returns-this-structure
 * 
 * EXCEPTION:
 *      PInvokeStackImbalance occurred - Additional information: A call to PInvoke function 'ConsoleApplication1!IHIL.HidFunctions::HID_GetOutputReportSize' has unbalanced the stack. This is likely because the managed PInvoke signature does not match the unmanaged target signature. Check that the calling convention and parameters of the PInvoke signature match the target unmanaged signature.
 *      http://stackoverflow.com/questions/2390407/pinvokestackimbalance-c-sharp-call-to-unmanaged-c-function/2738125#comment2825285_2738125
 *      -> use signature [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)] 
 * 
 * Project Properties to be changed:
 * 
 * Build: allow unsafe
 * BadFormatException: verify Build > Platform Target (x86)
 * 
 * 
 */
namespace IHIL
{
    public class HidFunctions
    {
        //// private static UInt32 DISPOSABLE_ADDRESS = 395123123;
        private static int BOOL_TRUE = 1;
        //// public string[] ConnectedHids { get; private set; }
        //public int ConnectedDeviceIndex { get; private set; }

        //public HidFunctions(string name)
        //{
        //    HID_Init();

        //    int deviceCount = HidFunctions.HID_FindDevices();
        //    // string[] ConnectedHids = new string[deviceCount];
        //    ConnectedDeviceIndex = -1;

        //    for (int i = 0; i < deviceCount && ConnectedDeviceIndex == -1; i++)
        //    {
        //        if (GetName(i).Contains(name)) ConnectedDeviceIndex = i;
        //    }

        //    if (ConnectedDeviceIndex == -1) throw new Exception("Device" +  name + " could not be found");
        //}

        //~HidFunctions()
        //{
        //    HID_Close();
        //    HID_UnInit();
        //}

        /// <summary>
        /// Calls managed code from HidClientAdapter.HidFunctions to get device name at index.
        /// Throws exception if 
        /// </summary>
        /// <param name="deviceIndex">device index in device detail array</param>
        /// <returns>device name</returns>
        public static string GetName(int deviceIndex)
        {
            int stringSize = 256;
            StringBuilder name = new StringBuilder(stringSize);
            int success = HidFunctions.HID_GetName(deviceIndex, name, stringSize);

            if (success == BOOL_TRUE) return name.ToString();
            else throw new Exception("invalid index value");
        }
        
        public static bool Write (int deviceIndex, byte value, int position)
        {
            // copy data from buffer to memory at position indicated by pointer
            int    size  = HID_GetOutputReportSize(deviceIndex);
            byte[] buf   = new byte[size];
            IntPtr pnt   = Marshal.AllocHGlobal((int)size);

            if (position > size) return false;
            buf[position] = value;

            Marshal.Copy(buf, 0, pnt, (int)size);

            int success = HID_Write(pnt, (uint)size);

            // clean used memory before returning
            Marshal.FreeHGlobal(pnt);
            return success == BOOL_TRUE;
        }
        
        public static byte[] ReadArray (int deviceIndex)
        {
            int size = HID_GetInputReportSize(deviceIndex);
            return HID_Read(deviceIndex, (uint)size);
        }

        public static byte Read (int deviceIndex)
        {
            int size = HID_GetInputReportSize(deviceIndex);
            return HID_Read(deviceIndex, (uint)size)[1];
        }
        #region Adapted dll imports

        public static int HID_Write(byte[] buf, UInt32 sz)
        {
            IntPtr pnt = Marshal.AllocHGlobal((int)sz);
            Marshal.Copy(buf, 0, pnt, (int)sz);
            int aux = HID_Write(pnt, sz);
            Marshal.FreeHGlobal(pnt);
            return aux;
        }

        //public static byte [] HID_Read (int connectedDeviceIndex, StringBuilder buf, uint bufSize)
        public static byte[] HID_Read(int connectedDeviceIndex, uint bufSize)
        {
            byte[] res = null;
            // int size  = HID_GetInputReportSize(connectedDeviceIndex);
            // byte[] buf = new byte[size];
            // StringBuilder buf   = new StringBuilder(size);
            // string buf = "";

            IntPtr ptr = Marshal.AllocHGlobal((int)bufSize);
            int read = HID_Read(ptr, bufSize);
            if (read != 0)
            { 
                res = new byte[read];

                Marshal.Copy(ptr, res, 0, read);
            }
            Marshal.FreeHGlobal(ptr);
            //HID_Read (buf, bufSize);

            return res;

            //return buf.ToString();
        }

        #endregion

        #region Import from dll
        // to receive a struct from DLL with sequential layout
        // [StructLayout(LayoutKind.Sequential)]
        // public struct BOOL { }
        // ----------------------------
        // add annotation
        //      [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        // to functions that receive parameters

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll
        /// Initializes array with USB HID devices' data.
        /// </summary>
        [DllImport("HIDClient.dll")]
        public static extern void HID_Init();

        /*
        [DllImport("HIDClient.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Test();
        */

        /// <summary> Calls unmanaged code from HIDClient.dll to free device detail array. </summary>
        [DllImport("HIDClient.dll")]
        public static extern void HID_UnInit();

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to get amount of USB HID devices.
        /// </summary>
        /// <returns>
        /// Amount of found USB HID devices
        /// </returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_FindDevices();
        
        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to get device input buffer size.
        /// Input buffer is used to GET data from device.
        /// </summary>
        /// <param name="num">device number</param>
        /// <returns>buffer size</returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetInputReportSize(int num);

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to ger device output buffer size.
        /// Output report size determines size of array to SEND data to device.
        /// </summary>
        /// <param name="num">device number in device detail array</param>
        /// <returns>number of bytes in output report.</returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetOutputReportSize(int num);
        
        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to close communications with current selected device.
        /// </summary>
        [DllImport("HIDClient.dll")]
        public static extern void HID_Close();
        
        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to read from device.
        /// </summary>
        /// <param name="buf">array (pointer) to return buffer</param>
        /// <param name="sz">buffer size</param>
        /// <returns>
        /// 0 (FALSE) - Pending byte to read
        /// 0 (FALSE) - Incomplete reading
        /// >= 1 
        /// </returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]        
        private static extern int HID_Read( IntPtr buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz);

        /* //Comentado Robson
        public static extern int HID_Read
            (
            StringBuilder buf,
            [MarshalAs(UnmanagedType.U4)]    UInt32 sz 
//            , [MarshalAs(UnmanagedType.U4)]  UInt32 cnt
            );
            (
            [MarshalAs(UnmanagedType.LPStr)] String buf,
            [MarshalAs(UnmanagedType.U4)] UInt32 sz,
            [MarshalAs(UnmanagedType.U4)] UInt32 cnt
            );
         */

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to open USB HID device.
        /// Creates file wit read and write permissions to access device.
        /// </summary>
        /// <param name="num">device number in device detail array</param>
        /// <returns>
        /// 0 (FALSE) - Path could not be created for selected index
        /// 0 (FALSE) - Handler to file could not be created
        /// 1 (TRUE)  - Device created successfully
        /// </returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int HID_Open(int num);
        public static bool Open (int num) { return HID_Open(num) == BOOL_TRUE; }

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to get USB HID device name.
        /// </summary>
        /// <param name="num">device number in device detail array</param>
        /// <param name="buf">array (buffer) to write name</param>
        /// <param name="sz">array size</param>
        /// <returns>
        /// 0 (FALSE) if pointer to device detail data is null
        /// 0 (FALSE) if file handler is invalid
        /// 1 (TRUE)  if name could be caught
        /// </returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetName(int num, StringBuilder buf, int sz);

        /// <summary>
        /// Calls unmanaged code from HIDClient.dll to write to USB HID device.
        /// To get buffer size, use HID_GetOutputReportSize.
        /// </summary>
        /// <param name="buf">array (pointer to buffer) to be written.</param>
        /// <param name="sz">size of the array</param>
        /// <returns>
        /// </returns>
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        private static extern int HID_Write( IntPtr buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz/*, [MarshalAs(UnmanagedType.Int)] UInt32 cnt*/);

        #region Unused DLL functions
        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetFeatureReportSize(int num);
        */

        /*
        [DllImport("HIDClient.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetSelectedDevice();
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_GetFeature([MarshalAs(UnmanagedType.LPStr)] String  buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_SetFeature([MarshalAs(UnmanagedType.LPStr)] String  buf, [MarshalAs(UnmanagedType.U4)] UInt32 sz);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevCount();
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevSelected();
        */

        // extern "C" __declspec(dllexport) DWORD HID_DevDetailData_CbSize(int num);
        // extern "C" __declspec(dllexport) CHAR* HID_DevDetailData_DevicePath(int num);

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevDetailDataSz(int num);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevInputReportSz(int num);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevOutputReportSz(int num);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int HID_DevFeatureReportSz(int num);
        */

        /*
        [DllImport("HIDClient.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.U8)]
        public static extern ulong HID_DevDetailDate_cbSize(int num);
        */
        #endregion
        #endregion
    }
}
