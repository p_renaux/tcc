﻿#define USESTATIC

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;

namespace IHIL
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            uint[] Results = new uint[100]; 
            sw.Start();
            //Console.WriteLine("Escrevendo alguma coisa para matar tempo");
            sw.Stop();
            Results[0] = (uint)((double)1000 * sw.Elapsed.TotalMilliseconds);

            Console.WriteLine("tempo em ms = " + sw.Elapsed.TotalMilliseconds);

            int testUsb = 0;
            int success;
            // char[] readBuf = new char[512];
            StringBuilder readBuf;
            
            HidFunctions.HID_Init();
            int nDev = HidFunctions.HID_FindDevices();

            success = HidFunctions.HID_Open(testUsb);

            // int count = HidFunctions.HID_DevCount(); // unneccessary count
            StringBuilder name = new StringBuilder(256);
            success = HidFunctions.HID_GetName(testUsb, name, 256);
            Console.Out.WriteLine(name.ToString());

            int wr_size = HidFunctions.HID_GetOutputReportSize(testUsb);
            int rd_size = HidFunctions.HID_GetInputReportSize(testUsb);
            byte[] buf = new byte[wr_size];
            buf[1] = 0x0;
            int auxWrite = 0;
            HidFunctions.HID_Write(buf, (uint)wr_size);
            Console.Out.WriteLine(".");
            int count = 1;
            byte[] values = new byte[6];
            values[0] = 0;
            values[1] = 1;
            values[2] = 2;
            values[3] = 4;
            values[4] = 8;
            byte[] read = new byte[20];
            do
            {
                System.Threading.Thread.Sleep(1);
                buf[1] = values[count % 5];
                sw.Reset();
                sw.Start();
                HidFunctions.HID_Write(buf, (uint)wr_size);

                // while (true) { System.Threading.Thread.Sleep(100);  }
                do
                {
                    read = HidFunctions.HID_Read(testUsb, (uint)rd_size);

                } while (read == null);
                byte val = (byte) (read[1] & 0xF);
                //if (val != buf[1]) Console.Out.Write("*"); else Console.Out.Write("!");
                sw.Stop();
                Results[count] = (uint)((double)1000 * sw.Elapsed.TotalMilliseconds);
                count++;
            } while (count < 100);
            
            for (int i=1;i<100; i++) { Console.Out.Write(Results[i] + ","); }
            Console.Out.WriteLine("finished");
        }

    }
}
