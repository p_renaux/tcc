﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeMeasurement
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //Console.WriteLine("Escrevendo alguma coisa para matar tempo");
            sw.Stop();

            Console.WriteLine("tempo em ms = " + sw.Elapsed.TotalMilliseconds);
            //Console.WriteLine("" + Stopwatch.Frequency);
        }
    }
}
