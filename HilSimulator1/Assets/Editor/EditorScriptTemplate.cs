﻿/*
/// <summary>
/// USE OF THIS SCRIPT:
/// This script is a template of a script to define how to display another script in the Unity Inspector GUI.
/// Remove comment block at beginning at ending of the script.
///	NOTE: keep this script under (root)/Assets/Editor.
/// SOURCES:
///		https://docs.unity3d.com/ScriptReference/EditorGUI.html
///		https://docs.unity3d.com/ScriptReference/EditorGUILayout.html	
/// </summary>

#define EN_SERIALIZED_PROPS
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(EditedGUIScript))] // substitute EditedGUIScript by name of desired script
// [CanEditMultipleObjects]             // uncomment this line to edit multiple serialized properties

public class EditorScriptTemplate : Editor {
	// SerializedProperty property;
	EditedGUIScript egScript;
	
	void OnEnable()
	{

#if EN_SERIALIZED_PROPS
		// Setup the serialized properties (variables from script edited)
		// i.e.
		// property = serializedObject.FindProperty("ScriptPublicVariable");
#else
		egScript = (EditedGUIScript) target;
#endif
	}
	
	public override void OnInspectorGUI()
	{
	
#if EN_SERIALIZED_PROPS
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update ();
#endif

		// update serialized properties
		// show custom GUI controls
	}
	
	void ProgressBar (float value, string label)
	{
		// custom GUILayout progress bar
	}
}
*/