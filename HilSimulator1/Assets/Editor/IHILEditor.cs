﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(IhilUnityAdapter))] 
public class EditorScriptTemplate : Editor {
	// SerializedProperty property;
	IhilUnityAdapter ihilScript;
	bool DisplayUsbWrite;
	
	void OnEnable()
	{
		ihilScript = (IhilUnityAdapter) target;
	}
	
	public override void OnInspectorGUI()
	{
		string usbIndex = ihilScript.GetUsbIndex == -1? "Not yet connected" : "Connected to "+ihilScript.GetUsbIndex;
		string usbName  = ihilScript.GetUsbIndex == -1? "Connection expected to "+ihilScript.GetDeviceName : "Connected to "+ihilScript.GetDeviceName;
		string usbWrite = ihilScript.GetUsbIndex == -1? "Not yet connected" : "Write Buffer ("+ihilScript.GetWriteBuffer.Length.ToString()+")";
		
		EditorGUILayout.LabelField("USB Index: ", usbIndex);
		EditorGUILayout.LabelField("USB Name: ",  usbName);
		
		DisplayUsbWrite = EditorGUILayout.Foldout(DisplayUsbWrite, usbWrite);
		if  (DisplayUsbWrite)
		{
			EditorGUI.indentLevel++;
			for (int i = 0; i < ihilScript.GetWriteBuffer.Length; i++)
			{
				EditorGUILayout.LabelField("["+i.ToString()+"]",ihilScript.GetWriteBuffer[i].ToString());
			}
			EditorGUI.indentLevel--;
		}
	}
}
