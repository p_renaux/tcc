﻿using UnityEngine;
using System;
using System.Collections;

public class BaseSensor : MonoBehaviour{
	private IhilUnityAdapter Ihil;
	private static string	 IHIL_GAMEOBJECT_NAME = "IHIL";
	
	public void Start()
	{
		try 
		{
			Ihil = UnityEngine.GameObject.Find (IHIL_GAMEOBJECT_NAME).GetComponent<IhilUnityAdapter> ();
		}
		catch (Exception e){};
	}
	
	public void SendValue (byte value)
	{
		if (Ihil == null) 
			throw new UnityException("GameObject "+IHIL_GAMEOBJECT_NAME+" not present in simulated scene");
		if (Ihil.GetUsbIndex < 0)
			throw new MethodAccessException ("Cannot send data if there is no connection to device");
		Ihil.EnqueueWrite(value);
	}
}