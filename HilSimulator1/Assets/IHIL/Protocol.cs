﻿using UnityEngine;
using System.Collections;

public class Protocol {
	private const int SHIFT = 4;

	public enum Readings {
		LIGHT1_MASK = 1,
		LIGHT2_MASK = 2,
		LIGHT3_MASK = 4,
		LIGHT4_MASK = 8
	};

	public enum Commands {
		BUTTON1_PRESSED = 1,
		BUTTON2_PRESSED = 2,
		BUTTON3_PRESSED = 4,
		BUTTON4_PRESSED = 8,
		BUTTON_RELEASED= 0
	};
}
