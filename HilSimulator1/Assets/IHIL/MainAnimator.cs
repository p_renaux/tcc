﻿using UnityEngine;
using System.Collections;

public class MainAnimator : MonoBehaviour {
	private ToggleLight Light1, Light2, Light3, Light4;
	private Protocol.Readings previousReadCode = (Protocol.Readings) (-1);
	// Use this for initialization
	void Start () {
		Light1 = GameObject.Find ("Lightbulb01").GetComponent<ToggleLight>();
		Light2 = GameObject.Find ("Lightbulb02").GetComponent<ToggleLight>();
		Light3 = GameObject.Find ("Lightbulb03").GetComponent<ToggleLight>();
		Light4 = GameObject.Find ("Lightbulb04").GetComponent<ToggleLight>();
	}

	public bool Animate (byte code)
	{

		Protocol.Readings readCode = (Protocol.Readings)code;
		if (readCode == previousReadCode) return true;
		previousReadCode = readCode;

		if ( (((int) readCode) & ((int) Protocol.Readings.LIGHT1_MASK)) != 0)
			Light1.TurnOn ();
		else
			Light1.TurnOff ();

		if ((((int) readCode) & ((int) Protocol.Readings.LIGHT2_MASK)) != 0)
			Light2.TurnOn ();
		else
			Light2.TurnOff ();

		if ((((int) readCode) & ((int) Protocol.Readings.LIGHT3_MASK)) != 0)
			Light3.TurnOn ();
		else
			Light3.TurnOff ();

		if ((((int) readCode) & ((int) Protocol.Readings.LIGHT4_MASK)) != 0)
			Light4.TurnOn ();
		else
			Light4.TurnOff ();
		/*
		switch (readCode)
		{
			case Protocol.Readings.LIGHT1_OFF: Light1.TurnOff(); break;
			case Protocol.Readings.LIGHT1_ON:  Light1.TurnOn();  break;

			case Protocol.Readings.LIGHT2_OFF: Light2.TurnOff(); break;
			case Protocol.Readings.LIGHT2_ON:  Light2.TurnOn();  break;

			case Protocol.Readings.LIGHT3_OFF: Light3.TurnOff(); break;
			case Protocol.Readings.LIGHT3_ON:  Light3.TurnOn();  break;
			
			case Protocol.Readings.LIGHT4_OFF: Light4.TurnOff(); break;
			case Protocol.Readings.LIGHT4_ON:  Light4.TurnOn();  break;

			default: return false;
		}
		*/

		return true;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
