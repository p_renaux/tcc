// #define USE_GAMEOBJECT
using UnityEngine;
using System;
using System.Text;
using System.Collections;
using IHIL;

/*
 * Adding C++ DLLs in Unity:
 * 
 * at http://docs.unity3d.com/Manual/PluginsForDesktop.html - Add c++ dll to Assets/Plugins - PRO ONLY
 */
public class IhilUnityAdapter : MonoBehaviour
{

	private HidFunctions Hid;
	/// <summary>
	/// Index of device in hid device list.
	/// Key values:
	/// -1 - not connected
	/// </summary>
	private int	 		 USBIndex 		 = -1;
	private int 		 WritePosition 	 = 1;
	private static int	 WRITEBUFFERSIZE  = 10;
	private byte[]		 WriteBuffer		 = new byte[WRITEBUFFERSIZE];
	private int			 WriteBufferCursor= 0;
	private string		 DeviceName 	 = "IHID";
	private int 		 OUTPUTREPORTSIZE;
	private uint		 INPUTREPORTSIZE;
	private MainAnimator Animator;

	// accessors
	public int 		GetUsbIndex   {get {return USBIndex;}}
	public string	GetDeviceName {get {return DeviceName;}}
	public byte[]	GetWriteBuffer {get {return WriteBuffer;}}
	
	void Start()
	{
		HidFunctions.HID_Init ();
		HidFunctions.HID_FindDevices ();
		int count = (int) HidFunctions.HID_DevCount();

		for (int i = 0; i < count; i++)
		{
			StringBuilder listedNameBuilder = new StringBuilder(256);
			HidFunctions.HID_GetName(i, listedNameBuilder, 256);
			string listedName = listedNameBuilder.ToString();

			if (listedName.Contains(DeviceName)){
				USBIndex = i;
				HidFunctions.Open (USBIndex);

				OUTPUTREPORTSIZE = HidFunctions.HID_GetOutputReportSize(USBIndex);
				INPUTREPORTSIZE = (uint) HidFunctions.HID_GetInputReportSize(USBIndex);
				
				DeviceName = listedName;
				Debug.Log("Successfully connected! \n\n Connected to device: "+DeviceName+"\n at index: "+USBIndex+"\n\n");
			}
		}
		if (USBIndex == -1) Debug.LogError("Not connected");
		Animator = gameObject.GetComponent<MainAnimator> ();
	}

	// void HandleRead (byte read) { Animator.Animate (read);}

	/// <summary>
	/// <para>
	/// Adds to byte queue (size 10) value read.
	/// </para>
	/// <para>
	/// THROWS:
	///		InvalidOperationException 	Device not connected
	///		OutOfMemoryException		Byte queue is full
	/// </para>
	/// </summary>
	/// <returns><c>true</c>, if write was enqueued, <c>false</c> otherwise.</returns>
	/// <param name="send">Send.</param>
	public void EnqueueWrite (byte send)
	{
		if (USBIndex == -1) throw new InvalidOperationException ("Not connected");
		if (WriteBufferCursor == WRITEBUFFERSIZE) throw new OutOfMemoryException ("Byte queue full");

		WriteBuffer [WriteBufferCursor++] = send;
	}

	private bool SendWrite ()
	{
		if (WriteBufferCursor == 0)  { /* Debug.Log("No characters to send"); */ return false; }

		byte val = WriteBuffer[--WriteBufferCursor];
		Debug.Log("Calling Hid_Write ("+val+")");
		return HidFunctions.Write (USBIndex, val, WritePosition);
	}

	private byte GetRead()
	{
		/*
		byte[] read = HidFunctions.ReadArray (USBIndex);
		byte   readValue = (byte) (read [0] - 128); // out of range
		return readValue;
		*/

		byte[] read = HidFunctions.HID_Read(USBIndex, INPUTREPORTSIZE);

		if (read != null)
		{
			// Debug.Log("Value read is: "+(read[1] & 0x7f)); // out of range
			byte value = (byte) (read[1] & 0x7f);
			Animator.Animate(value);
			return value;
		}

		return 0; // return read[0];
	}

	void OnGUI()
	{
		string USBConnectionOuput = "Connection successful";
		if (USBIndex < 0)	USBConnectionOuput = "Not connected";

		GUI.Label (new Rect (0, 0, 100, 100), USBConnectionOuput);
	}
	
	// Synchronous reading at same rate
	void FixedUpdate () 
	{
		// show once error
		if (USBIndex == -1) 
		{
			Debug.LogError ("Not Connected");
			USBIndex = -2;
		}
		
		// escape from update loop after showing log error
		if (USBIndex == -1) return;

		// byte[] read = HidFunctions.ReadArray (USBIndex);
		// byte readValue = read [0];
		SendWrite ();

		byte readInputs = (byte) (GetRead ());
		if (readInputs != 0) Debug.Log ("Value read is: " + readInputs);

	}
}
