﻿using UnityEngine;
using System.Collections;

public class ToggleLight : MonoBehaviour {
	private GameObject SphereOn;
	private GameObject SphereOff;
	private bool	   On		 = true;
	private float	   ScreenWidth;

	public void TurnOn() {
		if (On) return;

		SphereOff.renderer.enabled = false; // Destroy (SphereOff);
		SphereOn.renderer.enabled = true; // Instantiate (SphereOn);

		On = true;
	}

	public void TurnOff() {
		if (!On) return;

		SphereOn.renderer.enabled = false; //Destroy (SphereOn);
		SphereOff.renderer.enabled = true;// Instantiate (SphereOff);

		On = false;
	}
	// Use this for initialization
	void Start () 
	{
		SphereOn = gameObject.transform.FindChild ("SphereOn").gameObject;
		SphereOff = gameObject.transform.FindChild("SphereOff").gameObject;

		TurnOff ();

		ScreenWidth = gameObject.transform.lossyScale.x;
		float reposition = ScreenWidth * 0.75f;
		gameObject.transform.Translate (new Vector3 (reposition, 0));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
