﻿using UnityEngine;
using System.Collections;

public class SpecSensor : BaseSensor {
	public Protocol.Commands 	MouseDownCode;
	private Protocol.Commands	MouseUpCode = Protocol.Commands.BUTTON_RELEASED;
	// Use this for initialization
	void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown(){base.SendValue((byte) MouseDownCode);}
	
	void OnMouseUp(){base.SendValue((byte) MouseUpCode);}
	
}
