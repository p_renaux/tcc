﻿using UnityEngine;
using System.Collections;

public class PressAnimation : MonoBehaviour 
{
	private GameObject 		 ButtonReleased;
	private GameObject 		 ButtonPressed;
	private IhilUnityAdapter Ihil;
	public Protocol.Commands PressCode;
	private Protocol.Commands ReleaseCode = Protocol.Commands.BUTTON_RELEASED;

	// Use this for initialization
	void Start () {
		ButtonPressed = gameObject.transform.FindChild ("ButtonPressed").gameObject;
		ButtonReleased = gameObject.transform.FindChild ("ButtonReleased").gameObject;

		Ihil = GameObject.Find ("IHIL").GetComponent<IhilUnityAdapter> ();
	}
	
	// Update is called once per frame void Update () { }

	void OnMouseDown(){
		ButtonPressed.transform.renderer.enabled = true;
		ButtonReleased.transform.renderer.enabled = false;

		Ihil.EnqueueWrite ((byte)PressCode);
	}

	void OnMouseUp(){
		ButtonPressed.transform.renderer.enabled = false;
		ButtonReleased.transform.renderer.enabled = true;

		Ihil.EnqueueWrite ((byte)ReleaseCode);
	}
}
